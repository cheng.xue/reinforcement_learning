#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 24 13:59:27 2019

@author: cheng xue
"""
import gym
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from torch.distributions import Categorical
from torch.nn.functional import normalize

#from torch.utils.tensorboard import SummaryWriter
from collections import deque

class Params:
    NUM_EPOCHS = 5000
    ALPHA = 1e-4       # learning rate
    BATCH_SIZE = 4096     # how many episodes we want to pack into an epoch
    GAMMA = 1        # discount rate
    HIDDEN_SIZE = 64    # number of hidden nodes we have in our dnn
    BETA = 0.1          # the entropy bonus multiplier


# Q-table is replaced by a neural network
class Agent(nn.Module):
    '''
    class for the agent, with input of observation state, 
    output of a particular action
    '''
    def __init__(self, observation_space_size: int, action_space_size: int, hidden_size: int):
        super(Agent, self).__init__()


        self.net = nn.Sequential(
            nn.Linear(in_features=observation_space_size, out_features=hidden_size, bias=True),
            nn.PReLU(),
            nn.Linear(in_features=hidden_size, out_features=hidden_size, bias=True),
            nn.PReLU(),
            nn.Linear(in_features=hidden_size, out_features=action_space_size, bias=True),
            nn.Softmax(dim=-1)
        )

    def forward(self, x):
        x = normalize(x)
        #x = x/255
        x = self.net(x) 
        
        return x # outputs n x 2


class PolicyGradient():
    
    def __init__(self, problem: str = "CartPole", use_cuda: bool = False):
        self.NUM_EPOCHS = Params.NUM_EPOCHS
        self.ALPHA = Params.ALPHA
        self.BATCH_SIZE = Params.BATCH_SIZE
        self.GAMMA = Params.GAMMA
        self.HIDDEN_SIZE = Params.HIDDEN_SIZE
        self.BETA = Params.BETA
        self.DEVICE = torch.device('cuda' if torch.cuda.is_available() and use_cuda else 'cpu')
        # create the environment
        self.env = gym.make(problem)

        # the agent driven by a neural network architecture
        self.agent = Agent(observation_space_size=self.env.observation_space.shape[0],
                           action_space_size=self.env.action_space.n,
                           hidden_size=self.HIDDEN_SIZE).to(self.DEVICE)
        
        self.adam = optim.Adam(params=self.agent.parameters(), lr=self.ALPHA)

        self.total_rewards = deque([], maxlen=100)

        # flag to figure out if we have render a single episode current epoch
        self.finished_rendering_this_epoch = False
 
    def play_episode(self, episode: int):
        """
            Plays an episode of the environment.
            episode: the episode counter
            Returns:
                sum_weighted_log_probs: the sum of the log-prob of an action multiplied by the reward-to-go from that state
                episode_logits: the logits of every step of the episode - needed to compute entropy for entropy bonus
                finished_rendering_this_epoch: pass-through rendering flag
                sum_of_rewards: sum of the rewards for the episode - needed for the average over 200 episode statistic
        """
        # reset the environment to a random initial state every epoch
        state = self.env.reset()
        
        # initialize the episode arrays
        episode_actions = torch.empty(size=(0,), dtype=torch.long, device=self.DEVICE)
        episode_logits = torch.empty(size=(0,), device=self.DEVICE)
        average_rewards = np.empty(shape=(0,), dtype=np.float)
        episode_rewards = np.empty(shape=(0,), dtype=np.float)

        # episode loop
        while True:

            # render the environment for the first episode in the epoch
            if not self.finished_rendering_this_epoch:
                self.env.render()

            # get the action logits from the agent - (preferences) 
            action_prob = self.agent(torch.tensor(state).float().unsqueeze(dim=0).to(self.DEVICE)) #input 1 x state_dim, output 1 x action_dim
            #print('action_prob:',action_prob)
            sampler = Categorical(action_prob)
            
            action = sampler.sample()
            
            episode_logits = torch.cat((episode_logits,sampler.log_prob(action)), dim = 0)


            # append the action to the episode action list to obtain the trajectory
            # we need to store the actions and logits so we could calculate the gradient of the performance
            episode_actions = torch.cat((episode_actions, action), dim=0)

            # take the chosen action, observe the reward and the next state
            state, reward, done, life = self.env.step(action=action.cpu().item())
            
            if 'ale.lives' in life:
                if life['ale.lives'] != 3:
                    done = True
                

            # append the reward to the rewards pool that we collect during the episode
            # we need the rewards so we can calculate the weights for the policy gradient
            # and the baseline of average
            episode_rewards = np.concatenate((episode_rewards, np.array([reward])), axis=0)

            # here the average reward is state specific
            average_rewards = np.concatenate((average_rewards,
                                              np.expand_dims(np.mean(episode_rewards), axis=0)),
                                             axis=0)

            # the episode is over
            if done:
                #print('episode_actions:',episode_actions)
                # increment the episode
                episode += 1

                # turn the rewards we accumulated during the episode into the rewards-to-go:
                # earlier actions are responsible for more rewards than the later taken actions
                discounted_rewards_to_go = PolicyGradient.get_discounted_rewards(rewards=episode_rewards,
                                                                                 gamma=self.GAMMA)
                discounted_rewards_to_go -= average_rewards  # baseline - state specific average
                #print('discounted_rewards_to_go')

                # # calculate the sum of the rewards for the running average metric
                sum_of_rewards = np.sum(episode_rewards)
                #print('discounted_rewards_to_go: ',discounted_rewards_to_go)
                #print('episode_logits: ',episode_logits)

                # weight the episode log-probabilities by the rewards-to-go
                episode_weighted_log_probs = episode_logits * \
                    torch.tensor(discounted_rewards_to_go).float().to(self.DEVICE)
                    
                #print('episode_weighted_log_probs: ',episode_weighted_log_probs)

                # calculate the sum over trajectory of the weighted log-probabilities
                sum_weighted_log_probs = torch.sum(episode_weighted_log_probs).unsqueeze(dim=0)
                #print('sum_weighted_log_probs: ',sum_weighted_log_probs)

                # won't render again this epoch
                self.finished_rendering_this_epoch = True

                return sum_weighted_log_probs, episode_logits, sum_of_rewards, episode
        
    def calculate_loss(self, weighted_log_probs: torch.Tensor) -> (torch.Tensor, torch.Tensor):
        """
            Calculates the policy "loss" and the entropy bonus
            Args:
                epoch_logits: logits of the policy network we have collected over the epoch
                weighted_log_probs: loP * W of the actions taken
            Returns:
                policy loss + the entropy bonus
                entropy: needed for logging
        """
        policy_loss = -1 * torch.mean(weighted_log_probs)


        return policy_loss

    @staticmethod
    def get_discounted_rewards(rewards: np.array, gamma: float) -> np.array:
        """
            Calculates the sequence of discounted rewards-to-go.
            Args:
                rewards: the sequence of observed rewards
                gamma: the discount factor
            Returns:
                discounted_rewards: the sequence of the rewards-to-go
        """
        discounted_rewards = np.empty_like(rewards, dtype=np.float)
        for i in range(rewards.shape[0]):
            gammas = np.full(shape=(rewards[i:].shape[0]), fill_value=gamma)
            discounted_gammas = np.power(gammas, np.arange(rewards[i:].shape[0]))
            discounted_reward = np.sum(rewards[i:] * discounted_gammas)
            discounted_rewards[i] = discounted_reward
        return discounted_rewards

    def solve_environment(self):
        """
            The main interface for the Policy Gradient solver
        """
        # init the episode and the epoch
        episode = 0
        epoch = 0

        # init the epoch arrays
        # used for entropy calculation
        epoch_weighted_log_probs = torch.empty(size=(0,), dtype=torch.float, device=self.DEVICE)

        while True:

            # play an episode of the environment
            (episode_weighted_log_prob_trajectory,
             episode_logits,
             sum_of_episode_rewards,
             episode) = self.play_episode(episode=episode)


            # after each episode append the sum of total rewards to the deque
            self.total_rewards.append(sum_of_episode_rewards)

            # append the weighted log-probabilities of actions
            epoch_weighted_log_probs = torch.cat((epoch_weighted_log_probs, episode_weighted_log_prob_trajectory),
                                                 dim=0)


            # if the epoch is over - we have epoch trajectories to perform the policy gradient
            if episode >= self.BATCH_SIZE:

                # reset the rendering flag
                self.finished_rendering_this_epoch = False

                # reset the episode count
                episode = 0

                # increment the epoch
                epoch += 1
                #print('epoch_weighted_log_probs:',epoch_weighted_log_probs)
                # calculate the loss
                loss = self.calculate_loss(weighted_log_probs=epoch_weighted_log_probs)
                print('loss:',loss)
                # zero the gradient
                self.adam.zero_grad()

                # backprop
                loss.backward()

                # update the parameters
                self.adam.step()

                # feedback
                print( "\r Epoch: {}, Avg Return per Epoch: {:.3f}".format(epoch,np.mean(self.total_rewards)),
                      end="",
                      flush=True)

#                self.writer.add_scalar(tag='Average Return over 100 episodes',
#                                       scalar_value=np.mean(self.total_rewards),
#                                       global_step=epoch)
#
#                self.writer.add_scalar(tag='Entropy',
#                                       scalar_value=entropy,
#                                       global_step=epoch)

                # reset the epoch arrays
                # used for entropy calculation
                epoch_weighted_log_probs = torch.empty(size=(0,), dtype=torch.float, device=self.DEVICE)

                # check if solved
#                if np.mean(self.total_rewards) > 485:
#                    print('\nSolved!')
#                    break

        # close the environment
        self.env.close()

        # close the writer
        #self.writer.close()

def main():
    #
    env = 'SpaceInvaders-ram-v0'
    use_cuda = False

    #assert(env in ['CartPole', 'LunarLander'])

    policy_gradient = PolicyGradient(problem=env, use_cuda=use_cuda)
    policy_gradient.solve_environment()


if __name__ == "__main__":
    main()       